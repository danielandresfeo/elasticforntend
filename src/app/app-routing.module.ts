import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonasComponent} from "./components/personas/personas.component";

const routes: Routes = [
  {
    path:'personas',
    component: PersonasComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
