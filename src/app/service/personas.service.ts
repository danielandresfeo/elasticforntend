import { Injectable } from '@angular/core';
import {Persona} from "../models/Persona.model";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {
  selectedPersona: Persona = new Persona();
  uri = 'http://localhost:8081';
  constructor(private http: HttpClient) { }

   get()
  {
    return this.http.get<any>(`${this.uri}/listar`);
  }

   insert(persona)
  {
    let formData: FormData = new FormData();
    formData.append('cedula', persona.cedula);
    formData.append('nombre', persona.nombre);
    formData.append('apellido', persona.apellido);
    return this.http.post<any>(`${this.uri}/crear`,formData)
  }


   update(persona)
  {
    let formData: FormData = new FormData();
    formData.append('id', persona.id);
    formData.append('cedula', persona.cedula);
    formData.append('nombre', persona.nombre);
    formData.append('apellido', persona.apellido);
   return this.http.post<any>(`${this.uri}/editar`,formData)
  }

  delete(id){
    let formData: FormData = new FormData();
    formData.append('id', id);
    return this.http.post<any>(`${this.uri}/borrar`,formData)
  }



}
