import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasService } from '../../service/personas.service';

describe('PersonasComponent', () => {
  let component: PersonasService;
  let fixture: ComponentFixture<PersonasService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonasService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
