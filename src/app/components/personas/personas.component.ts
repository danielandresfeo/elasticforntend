import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PersonasService } from '../../service/personas.service';
@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
  public personas: [];
  public valid: false;
  constructor(
    public PersonasService: PersonasService
  ) { }

  ngOnInit() {
    this.updatelist()
    this.resetForm();
  }
  populate(data){
    this.personas =  data.hits.hits;
    console.log(this.personas);
  }

  updatelist(data?){
    this.PersonasService.get().subscribe( data => {
      //if(data != null)
        //this.populate(data)
      console.log(data);
      this.personas = data.hits.hits;
    });
  }

  onEdit(persona) {
    this.PersonasService.selectedPersona = {
      id: persona._id,
      nombre: persona._source.nombre,
      apellido: persona._source.apellido,
      cedula: persona._source.cedula
    }
  }
  onDelete(id) {
    this.PersonasService.delete(id).subscribe(data => {

      this.updatelist(data.hits.hits);
    });
  }

  onSubmit(personaForm: NgForm)
  {
    if (personaForm.valid){
    if(personaForm.value.id == null) {
      this.PersonasService.insert(personaForm.value).subscribe(data => {
        this.updatelist(data.hits.hits);
        console.log(data)
      });
    }
    else
      this.PersonasService.update(personaForm.value).subscribe(data => {
        this.updatelist(data.hits.hits);
      });

      //this.updatelist()
      this.resetForm(personaForm);
    }
    console.log('registro completo');
  }

  resetForm(productForm?: NgForm)
  {
    if(productForm != null)
      productForm.reset();

    //this.updatelist()
  }

}
